import { useEffect, useMemo, useRef, useState } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
//import { ThreeReactWrapper } from '../pwa-tools/tools/ThreeReactWrapper';
// import { AppRoutes, NavBar } from "../pwa-tools/utils/routing"
import { TouchControls } from '../pwa-tools/tools/TouchControls';
import { TouchInterface } from '../pwa-tools/tools/TouchInterface';
import { ThreeApp } from '../three-core-modules/core/ThreeApp';
import { BuildNum, NavBar } from '../pwa-tools/UI/elements';
import "../pwa-tools/pwa.css"
import { AppRoutes } from '../pwa-tools/utils/routing';
import { TerrainDemo } from './TerrainDemo';
import { GUI } from 'three/examples/jsm/libs/lil-gui.module.min.js';
import {DefaultTemplate} from './DefaultTemplate'
//import "./styles.css"

const BASENAME = process.env.PUBLIC_URL

// Entry point
const App = () => {
    return (
        <BrowserRouter basename={BASENAME}>
            {/* <Routes >
                <Route index element={<ThreeReactWrapper appClass={DemoTemplate} />} />
            </Routes> */}
            <Entries />
        </BrowserRouter>
    );
}

/**
 * Apps entries index 
 */
const Entries = () => {
    return (
        <div className="App">
            <NavBar />
            <div className="AppItems">
                <AppRoutes style={{ position: "absolute" }}>
                    {/* <LoadingScreen /> */}
                    <ThreeReactWrapper<DefaultTemplate> classObj={DefaultTemplate} />
                    <ThreeReactWrapper<TerrainDemo> classObj={TerrainDemo} />
                </AppRoutes>
            </div>
        </div>
    )
}

const ThreeGui = ({ instance }) => {
    // const guiRef = useRef();
    const domRef = useRef();

    useEffect(() => {
        // gui
        const gui = new GUI({ container: domRef.current });
        instance.initGui(gui);
        // gui.add(api, 'method', Method).onChange(initMesh);
        // gui.add(api, 'count', 1, 10000).step(1).onChange(initMesh);

        // const perfFolder = gui.addFolder('Performance');
        // guiStatsEl = document.createElement('div');
        // guiStatsEl.classList.add('gui-stats');
        // perfFolder.$children.appendChild(guiStatsEl);
        // perfFolder.open();
    }, [])

    return (<>
        <div className={"three-gui"} ref={domRef} />
    </>)
}
/**
 * Hook
 * contrary to normal JS function can acceess react hooks,
 * contrary to normal react function component, can return anything and accept any arguments
 * @param appClass 
 */
const useThree = (classObj, canvasRef) => {

    return useMemo(() => {
        // const stateProps = { setItem, setMode };
        //(demo as any).initState(stateProps);
        const instance = ThreeApp.instanciate(classObj, canvasRef)

        // if (instance?.state) {
        //     instance.state.isMobile = isMobile()
        //     console.log("is mobile: " + instance.state.isMobile)
        // }
        instance.onWindowResize();
        (instance as any).animate()
        // touchRef.current = instance.controls
        // lock screen in landscapte mode
        window.screen.orientation.lock("landscape").then(val => console.log(val))

        // setInstance(instance)
        return instance
    }, [classObj, canvasRef])

}

const useTouchControls = () => {

}

/**
 * React wrapper for launching three applications
 * Bridge between PWA <-> THREE
 * @returns 
 */
export function ThreeReactWrapper<Type>({ classObj }) {
    const canvasRef: any = useRef()
    const [instance, setInstance] = useState()
    // const instanceRef = useRef()   // access to instance
    const touchRef = useRef()   // access to controls
    const guiRef = useRef() // access to the GUI
    // const [item, setItem] = useState(-1);
    // const [mode, setMode] = useState(null);
    // const instance = useThree(TerrainDemo, canvasRef)
    // console.log(instance)

    useEffect(()=>{
        const instance = ThreeApp.instanciate(classObj, canvasRef)
        instance.onWindowResize();
        (instance as any).animate()
        setInstance(instance)
    }, [])
    

    // sync react and three states
    // useEffect(() => {
    //     ThreeApp.instances[0].state.item = item;
    //     ThreeApp.instances[0].state.mode = mode;
    // }, [item, mode])

    console.log("is ready? " + instance !== undefined)

    return (
        <>
            <div className="ThreeApp">
                {/* <Three ref={instanceRef}/> */}
                <canvas ref={canvasRef} />
                {/* <Gui guiRef={guiRef} /> */}
                {instance && <>
                    {/* <TouchControls touchRef={touchRef} >
                        <TouchInterface touchRef={touchRef} />
                    </TouchControls> */}
                    {/* <StatsWidget instance={instance} /> */}
                    <ThreeGui instance={instance}/>
                </>}
                {/* </> : <LoadingScreen />} */}
                {/* {currentBottle && mode === CONTROL_MODES.SELECTED && <OverlayV bottleCfg={currentBottle.config} onClose={closeOverlay} />} */}
            </div>
            {/* <DebugInfos /> */}
        </>

    );
}

export default App
