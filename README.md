# three-experiments

## Usage

Automatic deployment on gitpod by prefixing this repo url with `gitpod.io/#`,

Manual setup by redoing steps described in `.gitpod.yml` file

More info about cra template: `cra_template_customization/HOWTO.md` 

## Branches (WIP)
- canyon-demo
- map-terrain
- voxels-demo
