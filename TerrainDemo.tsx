import * as THREE from "three";
import { ThreeDemoApp } from "../three-core-modules/core/ThreeApp";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { Light, Mesh, MeshBasicMaterial, Vector3 } from "three";
import { ROCK2, SAND } from "../three-resources/catalogs/Materials";
import { ModelsFactory, RobotExpressiveModel, Xbot } from "../three-resources/catalogs/Models";
import { Player } from "../three-core-modules/misc/Player";
import * as CANNON from "cannon-es";
import Mousetrap from "mousetrap";
import "./styles.css"
import { noiseGen } from "../three-core-modules/procedural/ProcGen";
import { SimplexNoise } from "three/examples/jsm/math/SimplexNoise";
import { Material } from "material/Material";
import { TerrainGeometry } from "../three-core-modules/terrains/Terrain";
const CLASS_NAME = "TerrainDemo"

const PLAYER_MAX_SPEED = 0.35; // m/s
const PLAYER_ROT_SPEED = 0.025;

const TerrainType = {
  NOISE: 'NOISE',
  WAVE: 'WAVE',
  WAVE2: 'WAVE2',
};

const simplexNoise = new SimplexNoise()

const noiseAmplCorrection = 0.01
const noiseTimeScale = 0.5
const noiseDensityCorrection = 10

const speedAttenuation = 0.1
const lightAnimSpeed = .25

export class TerrainDemo extends ThreeDemoApp {
  static Name = CLASS_NAME;
  terrain: Mesh<TerrainGeometry>
  light: Light
  config = {
    terrain: {
      type: TerrainType.NOISE,
      size: 512,
      resolution: 32,
      density: 10,
      amplitude: 20,
      speed: 1,
      offset: 1,
      wireframe: false,
      refreshRate: 10 // times per sec
    },
    light: {
      animate: true,
    }
  };

  async init(initParams?): Promise<void> {
    super.init(initParams);
    console.log(`[${CLASS_NAME}] Init`);

    // Init Camera
    this.camera.position.y = 10;
    this.camera.position.z = 100;

    this.initDebugTools(this.config.terrain.size, this.config.terrain.resolution);
    this.initControls();
    this.addLights();
    await this.populate();
    console.log("continue initializing")
    this.initPlayerControls()
  }

  initGui(gui) {
    console.log(gui)
    const terrainFolder = gui.addFolder('Terrain');

    terrainFolder.add(this.config.terrain, 'type', TerrainType).onChange(() => this.updateDatasource());
    terrainFolder.add(this.config.terrain, 'speed', 0.0, 5, .25).onChange(val => console.log(val));
    terrainFolder.add(this.config.terrain, 'amplitude', 1, 50, 1).onChange(val => this.updateDatasource());
    terrainFolder.add(this.config.terrain, 'density', 1, 20, 1).onChange(val => this.updateDatasource());
    terrainFolder.add(this.config.terrain, 'resolution', 16, 128, 16).onChange(val => this.updateDatasource());
    terrainFolder.add(this.config.terrain, 'size', 64, 1024, 64).onChange(val => this.updateDatasource());
    terrainFolder.add(this.config.terrain, 'wireframe').onChange(val => (this.terrain.material as MeshBasicMaterial).wireframe = val);

    terrainFolder.open()

    const lightFolder = gui.addFolder('Lights');
    lightFolder.add(this.config.light, 'animate').onChange(val => console.log("light animation: " + this.config.light.animate));
  }

  initPlayerControls(customBindings = {}) {
    this.controls.joyLeft = { x: 0, y: 0, autoRest: true }
    this.controls.joyRight = { x: 0, y: 0, autoRest: false }

    // Mousetrap.bind("z", (e) => console.log(e), "keypress");
    Mousetrap.bind("z", (e) => (this.controls.joyLeft.y = -1), "keydown");
    Mousetrap.bind("z", (e) => (this.controls.joyLeft.y = 0), "keyup");
    Mousetrap.bind("s", (e) => (this.controls.joyLeft.y = 1), "keydown");
    Mousetrap.bind("s", (e) => (this.controls.joyLeft.y = 0), "keyup");
    Mousetrap.bind("q", (e) => (this.controls.joyLeft.x = -1), "keydown");
    Mousetrap.bind("q", (e) => (this.controls.joyLeft.x = 0), "keyup");
    Mousetrap.bind("d", (e) => (this.controls.joyLeft.x = 1), "keydown");
    Mousetrap.bind("d", (e) => (this.controls.joyLeft.x = 0), "keyup");

    // Mousetrap.bind("t", () => Player.current.trigElastic(this.highlightTriangle.center), "keypress");
    Mousetrap.bind("t", () => Player.current.releaseConstraints(), "keyup");

    Mousetrap.bind("space", (e) => this.controls.jump = 1, "keydown");
    // merge with predefined base class controls
    // this.controls = { ...this.controls, ...demoControls };
  }

  initControls(customBindings = {}) {
    super.initControls();
    const controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableRotate = true;
    controls.minDistance = 3;
    controls.maxDistance = 500;
    controls.maxPolarAngle = Math.PI * 0.55;
    controls.enableDamping = true;
    controls.screenSpacePanning = true;
    controls.target.y = 1.5;
    // controls.autoRotate = true;
    this.controls = controls;
    // merge with predefined base class controls
    // this.controls = { ...this.controls, ...demoControls };
  }

  updateDatasource() {
    // console.log(ds)
    let { type, offset, amplitude, resolution, speed, density } = this.config.terrain
    let datasource = (x, y, t) => 0
    // const densityRatio = density / resolution
    switch (type) {
      case TerrainType.NOISE:
        console.log("noise datasource")
        const timeScale = speed * noiseTimeScale
        const densityRange = noiseDensityCorrection * density
        const heightScale = noiseAmplCorrection * amplitude
        datasource = (x, y, t) => noiseGen(x * densityRange, y * densityRange, t * timeScale, simplexNoise) * heightScale
        this.terrain.geometry.datasource = datasource
        break;
      case TerrainType.WAVE:
        console.log("wave datasource")
        datasource = (x, y, t) => Math.sin(density * (x + t)) * amplitude
        this.terrain.geometry.datasource = datasource
        break;
      case TerrainType.WAVE2:
        console.log("wave 2 datasource")
        datasource = (x, y, t) => Math.sin(density * (x + t)) * Math.sin(density * (y + t)) * amplitude //(Math.sin((densityRatio * (x + t))) * Math.sin(20 * y / resolution) + offset) * amplitude
        this.terrain.geometry.datasource = datasource
        break;
    }
    //update terrain
    this.terrain.geometry.render(this.time)
  }

  async populate() {
    const { resolution, size, amplitude } = this.config.terrain
    const dataSource = (x, y, t) => 0
    const terrainGeom = new TerrainGeometry(size, size, resolution, resolution, dataSource)
    this.terrain = new Mesh(terrainGeom, SAND())
    this.terrain.receiveShadow = true
    this.terrain.castShadow = true
    this.scene.add(this.terrain)
    this.updateDatasource();

    const modelClass = Xbot;
    await ModelsFactory.create(modelClass).then((model) => {
      model.children[0].traverse(function (node) {
        if (node.isMesh) {
          console.log(node)
          node.castShadow = true;
          node.receiveShadow = true;
        }
      })
      const player = new Player(modelClass, this);
      player.initBody();
      player.initConstraints();
      // player.initSpotLight();
      player.castShadow = true
      this.scene.add(player);
      // this.player.add(this.orbital);
      console.log("Player created");
    });
    console.log("continue populate")
    const boxSize = 5
    const geometry = new THREE.BoxGeometry(boxSize, boxSize, boxSize);
    const cube = new THREE.Mesh(geometry, ROCK2(1));
    cube.position.set(5, 1, 5)
    cube.castShadow = true
    this.scene.add(cube);

    // Ground body
    const planeShape = new CANNON.Plane();
    const planeBody = new CANNON.Body({ mass: 0 });
    planeBody.position.set(0, 0, 0);
    planeBody.addShape(planeShape);
    planeBody.quaternion.setFromAxisAngle(
      new CANNON.Vec3(1, 0, 0),
      -Math.PI / 2
    );
    this.world.addBody(planeBody);

  }

  addLights() {
    const height = 5;
    const directional = new THREE.DirectionalLight(0xffffff, 2);
    directional.position.set(0, height, 100);
    directional.target.position.set(0, 0, 0);
    directional.castShadow = true;
    directional.shadow.bias = -0.001;
    directional.shadow.mapSize.width = 4096;
    directional.shadow.mapSize.height = 4096;
    directional.shadow.camera.near = 0.1;
    directional.shadow.camera.far = 500.0;
    directional.shadow.camera.near = 0.5;
    directional.shadow.camera.far = 500.0;
    directional.shadow.camera.left = 500;
    directional.shadow.camera.right = -500;
    directional.shadow.camera.top = 50;
    directional.shadow.camera.bottom = -50;
    this.light = directional;
    this.scene.add(directional);

    const ambient = new THREE.AmbientLight(0xffffff, 0.1);
    this.scene.add(ambient);
  }

  animate = () => {
    requestAnimationFrame(this.animate);
    // call base super class
    super.update()
    this.controls.update()
    if (this.config.terrain.speed)
      this.terrain.geometry.render(this.time * this.config.terrain.speed * speedAttenuation);
    if (this.config.light.animate) {
      this.light.position.setY((Math.sin(this.time * lightAnimSpeed) + 1) * 10)
      this.light.position.setX((Math.cos(this.time * lightAnimSpeed) + 1) * 20)
    }
    // update all player instances
    Player.instances.forEach((player) => {
      const pos = this.controls.joyLeft;
      const playerMove = new THREE.Vector3(pos.x, 0, pos.y);
      playerMove.normalize().multiplyScalar(PLAYER_MAX_SPEED);
      // playerPosition.multiplyScalar(0.4);
      // playerMove.applyEuler(this.orbital.rotation);
      player.update(this.delta, playerMove);
    });

    super.render()
  };
}