import * as THREE from "three";
import { ThreeDemoApp } from "../three-core-modules/core/ThreeApp";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
const CLASS_NAME = "DefaultTemplate"

export class DefaultTemplate extends ThreeDemoApp {
  static Name = CLASS_NAME;

  init(initParams?): void {
    super.init(initParams);
    console.log(`[${CLASS_NAME}] Init`);

    // Init Camera
    this.camera.position.y = 10;
    this.camera.position.z = 100;

    this.initDebugTools();
    this.populate();
    this.initControls();
  }

  initGui(){

  }

  initControls(customBindings = {}) {
    super.initControls();
    const controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableRotate = true;
    controls.minDistance = 3;
    controls.maxDistance = 500;
    controls.maxPolarAngle = Math.PI * 0.55;
    controls.enableDamping = true;
    controls.screenSpacePanning = true;
    controls.target.y = 1.5;
    this.controls = controls;
    // merge with predefined base class controls
    // this.controls = { ...this.controls, ...demoControls };
  }

  populate() {
    
  }

  addLights() {
    const directional = new THREE.DirectionalLight(0xffffff, 2);
    directional.position.set(50, 50, 50);
    directional.target.position.set(0, 0, 0);
    directional.castShadow = true;
    directional.shadow.bias = -0.001;
    directional.shadow.mapSize.width = 4096;
    directional.shadow.mapSize.height = 4096;
    directional.shadow.camera.near = 0.1;
    directional.shadow.camera.far = 500.0;
    directional.shadow.camera.near = 0.5;
    directional.shadow.camera.far = 500.0;
    directional.shadow.camera.left = 50;
    directional.shadow.camera.right = -50;
    directional.shadow.camera.top = 50;
    directional.shadow.camera.bottom = -50;
    this.scene.add(directional);

    const ambient = new THREE.AmbientLight(0xffffff, 0.5);
    this.scene.add(ambient);
  }

  animate = () => {
    requestAnimationFrame(this.animate);
    // call base super class
    super.update()
    this.controls.update()

    super.render()
  };
}